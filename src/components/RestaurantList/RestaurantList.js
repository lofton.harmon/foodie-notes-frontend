import React from 'react'
import styled from 'styled-components'
import axios from 'axios'
import RestaurantItem from './RestaurantItem'
import Container from '@material-ui/core/Container'

const ListContainer = styled(Container)`
    &&& {
        max-width: 600px;
    }
`

class RestaurantList extends React.Component {
    state = {
        resturants: []
    }

    componentDidMount () {
        axios.get('http://localhost:8000/restaurants/')
        .then(response => {
            this.setState({resturants: response.data})
        })
    }

    renderRestraunts = () => {
        return this.state.resturants.map(restaurant => <RestaurantItem restaurant={restaurant} />)
    }

    render () {
        return (
            <ListContainer>
                <h2>Local Restaurants</h2>
                {this.renderRestraunts()}
            </ListContainer>
        )
    }
}

export default RestaurantList