import React from 'react'
import styled from 'styled-components'
import Card from '@material-ui/core/Card'
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Rating from 'components/Rating/Rating'


const RestrauntCard = styled(Card)`
    & + & {
        margin-top: 20px;
    }
`

const AlignedGrid = styled(Grid)`
    &.MuiGrid-container.baseline {
        align-items: baseline;
    }

    &.MuiGrid-container.center {
        align-items: center;
    }
`

const Image = styled.img`
    width: 80px;
    height: auto;
`

class RestaurantItem extends React.Component {
    render () {
        const restaurant = this.props.restaurant
        return (
            <RestrauntCard>
                    <AlignedGrid container className="center" spacing={4}>
                        <Grid item xs={2}>
                            <Image src="https://baconmockup.com/400/400/"></Image>
                        </Grid>

                        <Grid item xs={10}>
                            <AlignedGrid container className="baseline">
                                <Grid item>
                                    <Typography variant="h6">{restaurant.name}</Typography>
                                </Grid>
                                <Grid item >
                                    <Typography variant="body2" color="textSecondary" >&nbsp;&mdash;&nbsp;{restaurant?.food_type_data?.name}</Typography>
                                </Grid>
                            </AlignedGrid>
                            <Rating rating={restaurant.user_rating} />
                            <Typography variant="body1">{restaurant.description}</Typography>
                        </Grid>
                    </AlignedGrid>
            </RestrauntCard>
        )
    }
}

export default RestaurantItem