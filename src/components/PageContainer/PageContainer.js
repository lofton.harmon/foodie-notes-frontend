import React from 'react'
import styled from 'styled-components'
import Container from '@material-ui/core/Container'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

const PaddedContainer = styled(Container)`
  padding-top: 44px;
`

class PageContainer extends React.Component {
  render () {
    return(
      <PaddedContainer>
        <AppBar>
          <Button color="inherit">
            <Typography variant="h6">
              Foodie Note
            </Typography>
          </Button>
        </AppBar>
        {this.props.children}
      </PaddedContainer>
    )
  }
}

export default PageContainer