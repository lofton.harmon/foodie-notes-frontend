import React from 'react'
import styled from 'styled-components'
import Star from '@material-ui/icons/Star'
import StarBorder from '@material-ui/icons/StarBorder'

const RatingContainer = styled.div`
    color: gold;
    font-size: 14px;
`


class Rating extends React.Component {
    renderStars = () => {
        const stars = []
        console.log('RATING', this.props.rating)
        for (let i = 1; i <= 5; i++) {
            if (i <= this.props.rating) {
                stars.push(<Star />)
            } else {
                stars.push(<StarBorder />)
            }
        }
        return stars
    }

    render() {
        return (
            <RatingContainer>
                {this.renderStars()}
            </RatingContainer>
        )
    }
}

export default Rating