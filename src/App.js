import React from 'react';
import logo from './logo.svg';
import './App.css';
import PageContainer from 'components/PageContainer/PageContainer'
import RestaurantList from 'components/RestaurantList/RestaurantList'

function App() {
  return (
    <div className="App">
      <PageContainer>
        <RestaurantList/>

      </PageContainer>
    </div>
  );
}

export default App;
